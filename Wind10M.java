package com.example.textile;


import javax.persistence.*;

@Entity
@Table(name = "wind10m")

public class Wind10M {
    @Id
    @GeneratedValue
    Long id;
    @Column(name = "direction")
    String direction;
    @Column(name = "speed")
    private Integer speed;

    public Wind10M() {
    }

    public Wind10M(Long id, String direction, Integer speed) {
        this.id = id;
        this.direction = direction;
        this.speed = speed;
    }

    public Long getId() {
        return id;
    }

    public Wind10M setId(Long id) {
        this.id = id;
        return this;
    }

    public String getDirection() {
        return direction;
    }

    public Wind10M setDirection(String direction) {
        this.direction = direction;
        return this;
    }

    public Integer getSpeed() {
        return speed;
    }

    public Wind10M setSpeed(Integer speed) {
        this.speed = speed;
        return this;
    }
}
