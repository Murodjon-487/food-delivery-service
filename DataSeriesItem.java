package com.example.textile;

import javax.persistence.*;

@Entity
@Table(name = "data_series")
public class DataSeriesItem {
    @Id
    @GeneratedValue
   private  Long id;

    @ManyToOne
    @JoinColumn(name = "astro_id",insertable = false,unique = false)
    private Astro astro;


    @Column(name = "timepoint")
    private Integer timepoint;
    @Column(name = "cloudcover")
    private Integer cloudcover;
    @Column(name = "seeing")
    private Integer seeing;
    @Column(name = "transparency")
    private Integer transparency;
    @Column(name = "lifted_index")
    private Integer lifted_index;
    @Column(name = "rh2m")
    private Integer rh2m;
    @ManyToOne(fetch = FetchType.EAGER,optional = false)
    private Wind10M wind10M;
    @Column(name = "temp2m")
    private Integer temp2m;
    @Column(name = "prec_type")
    private  String prec_type;

    public DataSeriesItem(){

    }

    public DataSeriesItem(Long id, Astro astro, Integer timepoint, Integer cloudcover, Integer seeing, Integer transparency, Integer lifted_index, Integer rh2m, Wind10M wind10M, Integer temp2m, String prec_type) {
        this.id = id;
        this.astro = astro;
        this.timepoint = timepoint;
        this.cloudcover = cloudcover;
        this.seeing = seeing;
        this.transparency = transparency;
        this.lifted_index = lifted_index;
        this.rh2m = rh2m;
        this.wind10M = wind10M;
        this.temp2m = temp2m;
        this.prec_type = prec_type;
    }

    public Long getId() {
        return id;
    }

    public DataSeriesItem setId(Long id) {
        this.id = id;
        return this;
    }

    public Astro getAstro() {
        return astro;
    }

    public DataSeriesItem setAstro(Astro astro) {
        this.astro = astro;
        return this;
    }

    public Integer getTimepoint() {
        return timepoint;
    }

    public DataSeriesItem setTimepoint(Integer timepoint) {
        this.timepoint = timepoint;
        return this;
    }

    public Integer getCloudcover() {
        return cloudcover;
    }

    public DataSeriesItem setCloudcover(Integer cloudcover) {
        this.cloudcover = cloudcover;
        return this;
    }

    public Integer getSeeing() {
        return seeing;
    }

    public DataSeriesItem setSeeing(Integer seeing) {
        this.seeing = seeing;
        return this;
    }

    public Integer getTransparency() {
        return transparency;
    }

    public DataSeriesItem setTransparency(Integer transparency) {
        this.transparency = transparency;
        return this;
    }

    public Integer getLifted_index() {
        return lifted_index;
    }

    public DataSeriesItem setLifted_index(Integer lifted_index) {
        this.lifted_index = lifted_index;
        return this;
    }

    public Integer getRh2m() {
        return rh2m;
    }

    public DataSeriesItem setRh2m(Integer rh2m) {
        this.rh2m = rh2m;
        return this;
    }

    public Wind10M getWind10M() {
        return wind10M;
    }

    public DataSeriesItem setWind10MDto(Wind10MDto wind10MDto) {
        this.wind10M = wind10M;
        return this;
    }

    public Integer getTemp2m() {
        return temp2m;
    }

    public DataSeriesItem setTemp2m(Integer temp2m) {
        this.temp2m = temp2m;
        return this;
    }

    public String getPrec_type() {
        return prec_type;
    }

    public DataSeriesItem setPrec_type(String prec_type) {
        this.prec_type = prec_type;
        return this;
    }
}
