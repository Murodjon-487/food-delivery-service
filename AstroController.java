package com.example.textile;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RestController("api/v1/astro")
public class AstroController {
@Autowired
    AstroService astroService;

@GetMapping
    ResponseEntity<?>findAll(){
    return new ResponseEntity(astroService.findAll(), HttpStatus.OK);
}

}
