package com.example.textile;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Wind10MDto {

    @JsonProperty("direction")
    String direction;
    @JsonProperty("speed")
    Integer speed;

    public Wind10MDto() {
    }

    public Wind10MDto(String direction, Integer speed) {
        this.direction = direction;
        this.speed = speed;
    }

    public String getDirection() {
        return direction;
    }

    public Wind10MDto setDirection(String direction) {
        this.direction = direction;
        return this;
    }

    public Integer getSpeed() {
        return speed;
    }

    public Wind10MDto setSpeed(Integer speed) {
        this.speed = speed;
        return this;
    }

    @Override
    public String toString() {
        return "Wind10MDto{" +
                "direction='" + direction + '\'' +
                ", speed=" + speed +
                '}';
    }






}
