package com.example.textile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AstroService {
@Autowired
    AstroRepo astroRepo;
public List<Astro>findAll(){
    return astroRepo.findAll();
    }
}
