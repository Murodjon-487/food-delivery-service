package com.example.textile;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "textile")
public class Textile {
    @Id
    @GeneratedValue
    Long id;
    @OneToMany(mappedBy = "Textile")
    private Set<Textile> textile = new HashSet<Textile>() {
        @Column(name = "dataSeries")
        ArrayList<DataSeriesDto> dataSeriesDto = new ArrayList<DataSeriesDto>();

        @Column(name = "product")
        String product;
        @Column(name = "init")
        String init;


    };

}
