package com.example.textile;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataSeriesDto {
    @JsonProperty("timepoint")
    Integer timepoint;
    @JsonProperty("cloudcover")
    Integer cloudcover;
    @JsonProperty("seeing")
    Integer seeing;
    @JsonProperty("transparency")
    Integer transparency;
    @JsonProperty("lifted_index")
    Integer lifted_index;
    @JsonProperty("rh2m" )
    Integer rh2m;
    @JsonProperty("wind10m")
    Wind10MDto wind10MDto;
    @JsonProperty("temp2m")
    Integer temp2m;
    @JsonProperty("prec_type")
    String prec_type;

    public DataSeriesDto() {
    }

    public DataSeriesDto(Integer timepoint, Integer cloudcover, Integer seeing, Integer transparency, Integer lifted_index, Integer rh2m, Wind10MDto wind10MDto, Integer temp2m, String prec_type) {
        this.timepoint = timepoint;
        this.cloudcover = cloudcover;
        this.seeing = seeing;
        this.transparency = transparency;
        this.lifted_index = lifted_index;
        this.rh2m = rh2m;
        this.wind10MDto = wind10MDto;
        this.temp2m = temp2m;
        this.prec_type = prec_type;
    }

    public Integer getTimepoint() {
        return timepoint;
    }

    public DataSeriesDto setTimepoint(Integer timepoint) {
        this.timepoint = timepoint;
        return this;
    }

    public Integer getCloudcover() {
        return cloudcover;
    }

    public DataSeriesDto setCloudcover(Integer cloudcover) {
        this.cloudcover = cloudcover;
        return this;
    }

    public Integer getSeeing() {
        return seeing;
    }

    public DataSeriesDto setSeeing(Integer seeing) {
        this.seeing = seeing;
        return this;
    }

    public Integer getTransparency() {
        return transparency;
    }

    public DataSeriesDto setTransparency(Integer transparency) {
        this.transparency = transparency;
        return this;
    }

    public Integer getLifted_index() {
        return lifted_index;
    }

    public DataSeriesDto setLifted_index(Integer lifted_index) {
        this.lifted_index = lifted_index;
        return this;
    }

    public Integer getRh2m() {
        return rh2m;
    }

    public DataSeriesDto setRh2m(Integer rh2m) {
        this.rh2m = rh2m;
        return this;
    }

    public Wind10MDto getWind10MDto() {
        return wind10MDto;
    }

    public DataSeriesDto setWind10MDto(Wind10MDto wind10MDto) {
        this.wind10MDto = wind10MDto;
        return this;
    }

    public Integer getTemp2m() {
        return temp2m;
    }

    public DataSeriesDto setTemp2m(Integer temp2m) {
        this.temp2m = temp2m;
        return this;
    }

    public String getPrec_type() {
        return prec_type;
    }

    public DataSeriesDto setPrec_type(String prec_type) {
        this.prec_type = prec_type;
        return this;
    }

    @Override
    public String  toString() {
        return "DataSeriesDto{" +
                "timepoint=" + timepoint +
                ", cloudcover=" + cloudcover +
                ", seeing=" + seeing +
                ", transparency=" + transparency +
                ", lifted_index=" + lifted_index +
                ", rh2m=" + rh2m +
                ", wind10MDto=" + wind10MDto +
                ", temp2m=" + temp2m +
                ", prec_type='" + prec_type + '\'' +
                '}';
    }
}
