package com.example.textile;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AstroRepo  extends JpaRepository<Astro,Long> {
}
