package com.example.textile;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class AstroDto {
@JsonProperty("product")
    String product;
@JsonProperty("init")
    String init;
@JsonProperty("dataseries")
    ArrayList<DataSeriesItemDto>dataSeries;
}
