package com.example.textile;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "astro")

public class Astro {
    @Id
    @GeneratedValue
    Long id;
    @Column(name = "product")
    String product;
    @Column(name = "init")
    String init;



    @OneToMany(mappedBy = "astro")
    private List<DataSeriesItem>dataSeries;

    public Astro() {
    }

    public Astro(String product, String init, ArrayList<DataSeriesItem> dataSeries) {
        this.product = product;
        this.init = init;
        this.dataSeries=dataSeries;
    }

    @Override
    public String   toString() {
        return "Astro{" +
                "product='" + product + '\'' +
                ", init='" + init + '\'' +
                ", dataSeries=" + dataSeries +
                '}';
    }
}
